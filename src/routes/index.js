import React from "react";

import { Switch, Route, BrowserRouter as Router } from "react-router-dom";
import Offers from "../pages/Offers";
import Administration from "../pages/Administration";

export default function Routes() {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={Offers} />
        <Route exact path="/administration" component={Administration} />
      </Switch>
    </Router>
  );
}
