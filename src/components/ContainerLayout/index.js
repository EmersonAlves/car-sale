import React from "react";
import { Layout } from "antd";
import "./styles.css";

export default function ContainerLayout({ children }) {
  return <Layout className="container-layout">{children}</Layout>;
}
