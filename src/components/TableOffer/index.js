import React, {
  useState,
  useEffect,
  useImperativeHandle,
  forwardRef,
} from "react";
import { Table, Row, Col, Divider, Popconfirm } from "antd";
import Search from "antd/lib/input/Search";
import api from "../../api";
import Mask from "../../utils/Mask";
import ModalOfferFormEdit from "../ModalOfferFormEdit";

function TableOffer(props, ref) {
  const [offer, setOffer] = useState(null);
  const [list, setList] = useState([]);
  const [filterTable, setFilterTable] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    getListOffers();
  }, []);

  useImperativeHandle(ref, () => ({
    update: () => {
      setLoading(true);
      getListOffers();
    },
  }));

  async function getListOffers() {
    const { data } = await api.get("offers");
    setList(
      data.map((item) => {
        return { ...item, price: Mask.maskMoney(item.price) };
      })
    );

    setFilterTable(
      data.map((item) => {
        return { ...item, price: Mask.maskMoney(item.price) };
      })
    );

    setLoading(false);
  }

  async function handleDelete(offer) {
    setLoading(true);
    await api.delete(`offers/${offer.id}`);
    getListOffers();
  }

  const columns = [
    {
      title: "Marca",
      dataIndex: "brand",
      key: "brand",
    },
    {
      title: "Modelo",
      dataIndex: "model",
      key: "model",
    },
    {
      title: "Ano",
      dataIndex: "year",
      key: "year",
    },
    ,
    {
      title: "Preço",
      dataIndex: "price",
      key: "price",
    },
    {
      title: "Cor",
      dataIndex: "color",
      key: "color",
    },
    {
      title: "Quilometragem",
      dataIndex: "mileage",
      key: "mileage",
      width: 10,
    },
    {
      title: "Placa",
      dataIndex: "license_plate",
      key: "license_plate",
    },
    {
      title: "Cidade",
      dataIndex: "city",
      key: "city",
    },
    {
      title: "Data de cadastro",
      dataIndex: "created",
      render: (text, item) => (
        <>{new Date(item.created_at._seconds * 1000).toLocaleString()}</>
      ),
    },
    {
      title: "",
      dataIndex: "operation",
      width: 100,
      render: (text, item) => (
        <>
          <Popconfirm
            title="Deseja excluir essa oferta?"
            okText="Sim"
            cancelText="Não"
            onConfirm={() => handleDelete(item)}
          >
            <a>Excluir</a>
          </Popconfirm>
          <a
            style={{ marginLeft: 25 }}
            href="#"
            onClick={(event) => {
              event.preventDefault();
              setOffer(item);
            }}
          >
            Editar
          </a>
        </>
      ),
    },
  ];

  return (
    <>
      <Divider />
      <Row>
        <Col className="gutter-row" xs={24} sm={24} md={12} lg={6} xl={6}>
          <Search
            placeholder="Pesquise por algo"
            onSearch={(value) => {
              setLoading(true);
              setTimeout(() => {
                setFilterTable(
                  list.filter((item) => {
                    return (
                      Object.keys(item).filter((attribute) => {
                        return `${item[attribute]}`
                          .toUpperCase()
                          .includes(value.toUpperCase());
                      }).length > 0
                    );
                  })
                );
                setLoading(false);
              }, 300);
            }}
            enterButton
          />
        </Col>
      </Row>
      <Divider />
      <Table dataSource={filterTable} columns={columns} loading={loading} />
      <ModalOfferFormEdit
        offer={offer}
        onClose={() => setOffer(null)}
        onUpdate={() => {
          setLoading(true);
          getListOffers();
        }}
      />
    </>
  );
}

TableOffer = forwardRef(TableOffer);

export default TableOffer;
