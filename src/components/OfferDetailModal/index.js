import React from "react";
import { Modal, Carousel, Divider } from "antd";
import "./styles.css";
import Mask from "../../utils/Mask";
import { Property } from "../ItemOffer";

export default function OfferDetailModal({ offer, onClose }) {
  return (
    <Modal title="Oferta" visible={!!offer} onCancel={onClose} footer={null}>
      {offer ? (
        <>
          <Carousel autoplay>
            {offer.images.map((image, index) => {
              return (
                <div key={index}>
                  <div
                    className="banner-offer"
                    style={{ backgroundImage: `url(${image})` }}
                  ></div>
                </div>
              );
            })}
          </Carousel>
          <Divider />
          <h4>R$ {Mask.maskMoney(offer.price)}</h4>
          <Property>
            <b>Marca:</b>
            {offer.brand}
          </Property>
          <Property>
            <b>Modelo:</b>
            {offer.model}
          </Property>
          <Property>
            <b>Ano:</b>
            {offer.year}
          </Property>
          <Property>
            <b>Cor:</b>
            {offer.color}
          </Property>
          <Property>
            <b>Placa:</b>
            {offer.license_plate}
          </Property>
          <Property>
            <b>Quilometragem:</b>
            {offer.mileage}
          </Property>
          <Property>
            <b>Data de cadastro:</b>
            {new Date(offer.created_at._seconds * 1000).toLocaleString()}
          </Property>
        </>
      ) : null}
    </Modal>
  );
}
