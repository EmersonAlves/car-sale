import React from "react";
import { Layout } from "antd";
import "./styles.css";

const { Content: ContentLayout } = Layout;

export default function Content({ children }) {
  return (
    <ContentLayout className="component-content">
      <div className="layout-content">{children}</div>
    </ContentLayout>
  );
}
