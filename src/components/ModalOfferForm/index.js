import React, { useState, useEffect } from "react";
import { Button, Modal, Form, Input, Upload, message, InputNumber } from "antd";
import { PlusOutlined } from "@ant-design/icons";
import InputMask from "../InputMask";
import { storage } from "../../services/firebase";
import api from "../../api";
import "./styles.css";

export default function ModalOfferForm({ onAdd }) {
  const [visible, setVisible] = useState(false);
  const [loading, setLoading] = useState(false);
  const [files, setFiles] = useState([]);
  const [form] = Form.useForm();

  function showModal() {
    setFiles([]);
    setVisible(true);
  }

  function handleCancel() {
    setFiles([]);
    setVisible(false);
  }

  function handleSubmit(values) {
    if (files.length === 0) return message.error("Envie pelo menos uma imagem");

    let regexLicensePlate = /^[a-zA-Z]{3}[0-9]{4}\b/;

    if (!regexLicensePlate.test(values.license_plate.replace("-", "")))
      return message.error("A placa do veículo está inválida");

    if (values.price === 0)
      return message.error("O valor do veículo deve ser maior que 0 (zero)");

    values.license_plate = values.license_plate.toUpperCase();

    setLoading(true);
    handleUpload(files).then((urls) => {
      api
        .post("offers", {
          ...values,
          images: urls,
        })
        .then(() => {
          message.success("Oferta cadastra com sucesso!");
          setVisible(false);
          setLoading(false);
          form.resetFields();
          onAdd();
        });
    });
  }

  const handleUpload = (files) => {
    return Promise.all(
      files.map((file) => {
        return new Promise((resolve, reject) => {
          const splits = file.name.split(".");
          const name = new Date().getTime() + "." + splits[splits.length - 1];

          const upload = storage.ref(`images/${name}`).put(file.originFileObj);
          upload.on(
            "state_changed",
            (snapshot) => {},
            (error) => {
              reject(error);
            },
            () => {
              storage
                .ref("images")
                .child(name)
                .getDownloadURL()
                .then((url) => {
                  resolve(url);
                });
            }
          );
        });
      })
    );
  };

  return (
    <>
      <Button type="primary" onClick={showModal}>
        Adicionar uma nova oferta
      </Button>
      <Modal
        visible={visible}
        title="Adicionar uma nova oferta"
        onCancel={handleCancel}
        footer={null}
      >
        <Form
          form={form}
          name="basic"
          layout="vertical"
          initialValues={{ remember: true }}
          onFinish={handleSubmit}
        >
          <Form.Item
            label="Marcar"
            name="brand"
            rules={[{ required: true, message: "Informe a marca do veículo" }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Modelo"
            name="model"
            rules={[{ required: true, message: "Informe o modelo do veículo" }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Ano"
            name="year"
            rules={[{ required: true, message: "Informe o ano do veículo" }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Cor"
            name="color"
            rules={[{ required: true, message: "Informe a cor do veículo" }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Quilometragem"
            name="mileage"
            rules={[
              { required: true, message: "Informe a quilometragem do veículo" },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Placa"
            name="license_plate"
            rules={[{ required: true, message: "Informe a placa do veículo" }]}
          >
            <InputMask mask="aaa-9999" />
          </Form.Item>
          <Form.Item
            label="Preço"
            name="price"
            rules={[{ required: true, message: "Informe o preço do veículo" }]}
          >
            <InputNumber
              formatter={(value) => {
                const numberPattern = /\d+/g;

                let result = `${value}`.replace(/\./g, "").match(numberPattern);

                if (Array.isArray(result)) result = result.join("");

                let number = parseInt(result);

                if (!isNaN(number)) {
                  return parseInt(number);
                }
              }}
              style={{ width: "100%" }}
            />
          </Form.Item>
          <Form.Item
            label="Cidade"
            name="city"
            rules={[{ required: true, message: "Informe a cidade do veículo" }]}
          >
            <Input />
          </Form.Item>
          <Form.Item>
            <UploadImage
              onChangeFiles={(files) => setFiles(files)}
              clear={!visible}
            />
          </Form.Item>
          <Form.Item className="options-modal">
            <Button type="primary" htmlType="submit" loading={loading}>
              Salvar
            </Button>
            <Button key="back" onClick={handleCancel} disabled={loading}>
              Cancelar
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
}

function UploadImage({ onChangeFiles, clear }) {
  const [previewVisible, setPreviewVisible] = useState(false);
  const [previewImage, setPreviewImage] = useState("");
  const [previewTitle, setPreviewTitle] = useState("");
  const [fileList, setFileList] = useState([]);

  useEffect(() => {
    if (clear) setFileList([]);
  }, [clear]);

  const handleChange = ({ fileList }) => {
    setFileList(
      fileList.filter((file) => {
        return /\.(jpe?g|jpg|png|gif|bmp)$/i.test(file.name);
      })
    );
  };

  const handleCancel = () => setPreviewVisible(false);

  const handlePreview = async (file) => {
    if (!file.url && !file.preview) {
      file.preview = await getBase64(file.originFileObj);
    }

    setPreviewImage(file.url || file.preview);
    setPreviewVisible(true);
    setPreviewTitle(
      file.name || file.url.substring(file.url.lastIndexOf("/") + 1)
    );
  };

  const uploadButton = (
    <div>
      <PlusOutlined />
      <div style={{ marginTop: 8 }}>Adicionar Imagem</div>
    </div>
  );

  return (
    <>
      <Upload
        action={null}
        listType="picture-card"
        fileList={fileList}
        onPreview={handlePreview}
        onChange={(result) => {
          onChangeFiles(result.fileList);
          handleChange(result);
        }}
        beforeUpload={(file) => {
          console.log(file.type);
          if (file.type !== "image/png" && file.type !== "image/jpeg") {
            message.error(`${file.name}  não é do tipo imagem`);
          }
          return false;
        }}
      >
        {fileList.length >= 8 ? null : uploadButton}
      </Upload>
      <Modal
        visible={previewVisible}
        title={previewTitle}
        footer={null}
        onCancel={handleCancel}
      >
        <img alt="example" style={{ width: "100%" }} src={previewImage} />
      </Modal>
    </>
  );
}

function getBase64(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });
}
