import React, { useState, useEffect } from "react";
import { Button, Modal, Form, Input, Upload, message, InputNumber } from "antd";
import { PlusOutlined } from "@ant-design/icons";
import InputMask from "../InputMask";
import Mask from "../../utils/Mask";
import { storage } from "../../services/firebase";
import api from "../../api";
import "./styles.css";

export default function ModalOfferFormEdit({ offer, onClose, onUpdate }) {
  const [visible, setVisible] = useState(false);
  const [loading, setLoading] = useState(false);
  const [files, setFiles] = useState([]);
  const [form] = Form.useForm();

  useEffect(() => {
    if (offer) {
      form.setFieldsValue({
        ...offer,
        price: Mask.removeMaskMoney(offer.price),
      });
      setVisible(true);
    }
  }, [offer]);

  function showModal() {
    setVisible(true);
  }

  function handleCancel() {
    setVisible(false);
    onClose();
  }

  function handleSubmit(values) {
    if (files.length === 0) return message.error("Envie pelo menos uma imagem");

    console.log(files);

    let regexLicensePlate = /^[a-zA-Z]{3}[0-9]{4}\b/;

    if (!regexLicensePlate.test(values.license_plate.replace("-", "")))
      return message.error("A placa do veículo está inválida");

    if (values.price === 0)
      return message.error("O valor do veículo deve ser maior que 0 (zero)");

    values.license_plate = values.license_plate.toUpperCase();

    setLoading(true);
    handleUpload(files).then((urls) => {
      api
        .put(`offers/${offer.id}`, {
          ...values,
          images: urls,
        })
        .then(() => {
          message.success("Oferta atualizada com sucesso!");
          setVisible(false);
          setLoading(false);
          onClose();
          onUpdate();
          form.resetFields();
        });
    });
  }

  const handleUpload = (files) => {
    return Promise.all(
      files.map((file) => {
        return new Promise((resolve, reject) => {
          if (file.status) {
            resolve(file.url);
          } else {
            const splits = file.name.split(".");
            const name = new Date().getTime() + "." + splits[splits.length - 1];

            const upload = storage
              .ref(`images/${name}`)
              .put(file.originFileObj);
            upload.on(
              "state_changed",
              (snapshot) => {},
              (error) => {
                reject(error);
              },
              () => {
                storage
                  .ref("images")
                  .child(name)
                  .getDownloadURL()
                  .then((url) => {
                    resolve(url);
                  });
              }
            );
          }
        });
      })
    );
  };

  return (
    <>
      <Modal
        visible={visible}
        title="Atualizar oferta"
        onCancel={handleCancel}
        footer={null}
      >
        <Form
          initialValues={offer}
          form={form}
          name="basic"
          layout="vertical"
          initialValues={{ remember: true }}
          onFinish={handleSubmit}
        >
          <Form.Item
            label="Marcar"
            name="brand"
            rules={[{ required: true, message: "Informe a marca do veículo" }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Modelo"
            name="model"
            rules={[{ required: true, message: "Informe o modelo do veículo" }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Ano"
            name="year"
            rules={[{ required: true, message: "Informe o ano do veículo" }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Cor"
            name="color"
            rules={[{ required: true, message: "Informe a cor do veículo" }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Quilometragem"
            name="mileage"
            rules={[
              { required: true, message: "Informe a quilometragem do veículo" },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Placa"
            name="license_plate"
            rules={[{ required: true, message: "Informe a placa do veículo" }]}
          >
            <InputMask mask="aaa-9999" />
          </Form.Item>
          <Form.Item
            label="Preço"
            name="price"
            rules={[{ required: true, message: "Informe o preço do veículo" }]}
          >
            <InputNumber
              formatter={(value) => {
                const numberPattern = /\d+/g;

                let result = `${value}`.replace(/\./g, "").match(numberPattern);

                if (Array.isArray(result)) result = result.join("");

                let number = parseInt(result);

                if (!isNaN(number)) {
                  return parseInt(number);
                }
              }}
              style={{ width: "100%" }}
            />
          </Form.Item>
          <Form.Item
            label="Cidade"
            name="city"
            rules={[{ required: true, message: "Informe a cidade do veículo" }]}
          >
            <Input />
          </Form.Item>
          <Form.Item>
            <UploadImage
              offer={offer}
              onChangeFiles={(files) => setFiles(files)}
            />
          </Form.Item>
          <Form.Item className="options-modal">
            <Button type="primary" htmlType="submit" loading={loading}>
              Atualizar
            </Button>
            <Button key="back" onClick={handleCancel} disabled={loading}>
              Cancelar
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
}

function UploadImage({ onChangeFiles, offer }) {
  const [previewVisible, setPreviewVisible] = useState(false);
  const [previewImage, setPreviewImage] = useState("");
  const [previewTitle, setPreviewTitle] = useState("");
  const [fileList, setFileList] = useState([]);

  useEffect(() => {
    if (offer) {
      setFileList(
        offer.images.map((image, index) => {
          return {
            uid: index * -1,
            name: "image-finished.png",
            status: "done",
            url: image,
          };
        })
      );

      onChangeFiles(
        offer.images.map((image, index) => {
          return {
            uid: index * -1,
            name: "image-finished.png",
            status: "done",
            url: image,
          };
        })
      );
    }
  }, [offer]);

  const handleChange = ({ fileList }) => {
    setFileList(
      fileList.filter((file) => {
        return /\.(jpe?g|jpg|png|gif|bmp)$/i.test(file.name);
      })
    );
  };

  const handleCancel = () => setPreviewVisible(false);

  const handlePreview = async (file) => {
    if (!file.url && !file.preview) {
      file.preview = await getBase64(file.originFileObj);
    }

    setPreviewImage(file.url || file.preview);
    setPreviewVisible(true);
    setPreviewTitle(
      file.name || file.url.substring(file.url.lastIndexOf("/") + 1)
    );
  };

  const uploadButton = (
    <div>
      <PlusOutlined />
      <div style={{ marginTop: 8 }}>Adicionar Imagem</div>
    </div>
  );

  return (
    <>
      <Upload
        action={null}
        listType="picture-card"
        fileList={fileList}
        onPreview={handlePreview}
        onChange={(result) => {
          onChangeFiles(result.fileList);
          handleChange(result);
        }}
        beforeUpload={(file) => {
          if (file.type !== "image/png") {
            message.error(`${file.name}  não é do tipo imagem`);
          }
          return false;
        }}
      >
        {fileList.length >= 8 ? null : uploadButton}
      </Upload>
      <Modal
        visible={previewVisible}
        title={previewTitle}
        footer={null}
        onCancel={handleCancel}
      >
        <img alt="example" style={{ width: "100%" }} src={previewImage} />
      </Modal>
    </>
  );
}

function getBase64(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });
}
