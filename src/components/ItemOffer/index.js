import React from "react";
import { Card } from "antd";
import Mask from "../../utils/Mask";
import "./styles.css";

export default function ItemOffer({ offer, onClick }) {
  return (
    <Card
      hoverable
      onClick={onClick}
      className="card-offer"
      cover={
        <div
          className="background-image-offer"
          style={{ backgroundImage: `url(${offer.images[0]})` }}
        ></div>
      }
    >
      <h4>R$ {Mask.maskMoney(offer.price)}</h4>
      <Property>
        <b>Marca:</b> {offer.brand}
      </Property>
      <Property>
        <b>Modelo:</b> {offer.model}
      </Property>
      <Property>
        <b>Ano:</b> {offer.year}
      </Property>
      <small>{offer.views} visualizações</small>
    </Card>
  );
}

export function Property({ children }) {
  return <p className="card-property">{children}</p>;
}
