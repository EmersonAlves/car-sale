import React from "react";
import { Layout, Menu } from "antd";
import { Link } from "react-router-dom";

const { Header } = Layout;

export default function Navbar() {
  return (
    <Header>
      <Menu
        theme="dark"
        mode="horizontal"
        defaultSelectedKeys={[window.location.pathname]}
      >
        <Menu.Item key="/">
          <Link to="/">Ofertas</Link>
        </Menu.Item>
        <Menu.Item key="/administration">
          <Link to="/administration">Administração</Link>
        </Menu.Item>
      </Menu>
    </Header>
  );
}
