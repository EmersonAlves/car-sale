import React from "react";
import { Input } from "antd";
import ReactInputMask from "react-input-mask";
import "./styles.css";

const InputMask = (props) => {
  return (
    <ReactInputMask {...props}>
      {(inputProps) => (
        <Input
          {...inputProps}
          disabled={props.disabled ? props.disabled : null}
          className="input-mask"
        />
      )}
    </ReactInputMask>
  );
};

export default InputMask;
