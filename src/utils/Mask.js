export default class Mask {
  static removeMaskMoney(value) {
    return parseFloat(value.replace(/\./g, "").replace(/\,/g, "."));
  }

  static maskMoney(value) {
    let number = value.toFixed(4).split(".");

    number = parseFloat(`${number[0]}.${number[1].slice(0, 2)}`);

    number = number.toFixed(2).split(".");
    number[0] = number[0].split(/(?=(?:...)*$)/).join(".");
    return number.join(",");
  }
}
