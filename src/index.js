import React from "react";
import ReactDOM from "react-dom";
import Routes from "./routes";
import "./index.css";
import "antd/dist/antd.css";

ReactDOM.render(
  <React.StrictMode>
    <Routes />
  </React.StrictMode>,
  document.getElementById("root")
);
