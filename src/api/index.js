import axios from "axios";

const api = axios.create({
  baseURL: "https://us-central1-car-sale-83952.cloudfunctions.net/api/",
});

export default api;
