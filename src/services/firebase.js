import firebase from "firebase/app";
import "firebase/storage";

const firebaseConfig = {
  apiKey: "AIzaSyBPNT8w8C8QHiXn6nn0elC-Pe3qqwvYU40",
  authDomain: "car-sale-83952.firebaseapp.com",
  databaseURL: "https://car-sale-83952.firebaseio.com",
  projectId: "car-sale-83952",
  storageBucket: "car-sale-83952.appspot.com",
  messagingSenderId: "629690908873",
  appId: "1:629690908873:web:427983c5efe54cbab6cd18",
};

firebase.initializeApp(firebaseConfig);

const storage = firebase.storage();

export { storage, firebase as default };
