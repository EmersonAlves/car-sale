import React, { useState, useEffect } from "react";
import ContainerLayout from "../../components/ContainerLayout";
import Navbar from "../../components/Navbar";
import Content from "../../components/Content";
import { List, Radio, Divider, Row, Col } from "antd";
import api from "../../api";
import ItemOffer from "../../components/ItemOffer";
import OfferDetailModal from "../../components/OfferDetailModal";
import { BarsOutlined, TableOutlined } from "@ant-design/icons";
import Mask from "../../utils/Mask";

export default function Offers() {
  const [offer, setOffer] = useState(null);
  const [list, setList] = useState([]);
  const [loading, setLoading] = useState(true);
  const [type, setType] = useState("grid");

  async function handleView(offer) {
    api.put(`offers/${offer.id}/views`);
  }

  useEffect(() => {
    async function getData() {
      const { data } = await api.get("offers");
      setList(data);
      setLoading(false);
    }

    getData();
  }, []);

  return (
    <ContainerLayout>
      <Navbar />
      <Content>
        <Radio.Group value={type} onChange={(e) => setType(e.target.value)}>
          <Radio.Button value="grid">
            <TableOutlined />
          </Radio.Button>
          <Radio.Button value="list">
            <BarsOutlined />
          </Radio.Button>
        </Radio.Group>
        <Divider />
        <List
          loading={loading}
          grid={
            type === "grid"
              ? {
                  gutter: 16,
                  xs: 1,
                  sm: 1,
                  md: 2,
                  lg: 4,
                  xl: 4,
                  xxl: 4,
                }
              : null
          }
          dataSource={list}
          renderItem={(item) => {
            if (type !== "grid")
              return (
                <List.Item
                  onClick={() => {
                    handleView(item);
                    setOffer(item);
                  }}
                >
                  <Row style={{ width: "100%", cursor: "pointer" }}>
                    <Col className="gutter-row" span={4}>
                      <img src={item.images[0]} width={50} />
                    </Col>
                    <Col className="gutter-row" span={4}>
                      {Mask.maskMoney(item.price)}
                    </Col>
                    <Col className="gutter-row" span={4}>
                      {item.brand}
                    </Col>
                    <Col className="gutter-row" span={4}>
                      {item.model}
                    </Col>
                    <Col className="gutter-row" span={4}>
                      {item.year}
                    </Col>
                    <Col className="gutter-row" span={4}>
                      Visualizações {item.views}
                    </Col>
                  </Row>
                </List.Item>
              );
            return (
              <List.Item>
                <ItemOffer
                  offer={item}
                  onClick={() => {
                    handleView(item);
                    setOffer(item);
                  }}
                />
              </List.Item>
            );
          }}
        />
        <OfferDetailModal offer={offer} onClose={() => setOffer(null)} />
      </Content>
    </ContainerLayout>
  );
}
