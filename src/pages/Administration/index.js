import React, { useRef } from "react";
import Navbar from "../../components/Navbar";
import ContainerLayout from "../../components/ContainerLayout";
import Content from "../../components/Content";
import ModalOfferForm from "../../components/ModalOfferForm";
import TableOffer from "../../components/TableOffer";

export default function Administration() {
  const refTable = useRef();
  return (
    <ContainerLayout>
      <Navbar />
      <Content>
        <ModalOfferForm
          onAdd={() => {
            refTable.current.update();
          }}
        />
        <TableOffer ref={refTable} />
      </Content>
    </ContainerLayout>
  );
}
